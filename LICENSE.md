# Discourse-Dashboard

_Web dashboard displaying data retrieved using Discourse API_

By:

* Xavier Arques <mailto:xavierarques@yahoo.com>
* François Bouchet <mailto:francoisbouchet@gmail.com>
* Emmanuel Raviart <mailto:emmanuel@raviart.com>

Copyright (C) 2018 Xavier Arques, François Bouchet, Paula Forteza & Emmanuel Raviart

https://framagit.org/parlement-ouvert/discourse-dashboard

> Discourse-Dashboard is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Discourse-Dashboard is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
