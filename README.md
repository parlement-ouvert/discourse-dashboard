# Discourse-Dashboard

_Web dashboard displaying data retrieved using Discourse API_

**CAUTION: This project has been renamed to _Vitrine_. Its development continues on another repository: https://framagit.org/eraviart/vitrine.**

**This repository is now deprecated, but development continues on [Vitrine repository](https://framagit.org/eraviart/vitrine).**

![Screenshot of a web site based on Discourse-Dashboard](https://forum.parlement-ouvert.fr/uploads/default/optimized/1X/a0a1d5850054eadca16d05a019b5165e08ed9c87_1_623x500.jpg)

_Discourse-Dashboard_ is a web application that generates dynamically the landing page of web site using data provided by the API of a [Discourse](https://www.discourse.org/) forum.

It can be used as the home page for a hackathon, to display the projects, the datasets, the participants, etc.

_Discourse-Dashboard_ is a kind of clone of [Hackdash](https://hackdash.org/), but instead of having its own back-office, it uses Discourse and benefits from it power and versatility.

Usages examples:
* [#dataFin hackathon](https://hackathon.parlement-ouvert.fr/)
* [bureau ouvert](https://parlement-ouvert.fr/)

_Discourse-Dashboard_ is free and open source software developped by, for and during le [_bureau ouvert_](https://parlement-ouvert.fr/).

* [software repository](https://framagit.org/parlement-ouvert/discourse-dashboard)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/parlement-ouvert/discourse-dashboard/blob/master/LICENSE.md)
