export async function fetchDiscourseCategory(component, url) {
  let category = null
  for (let page = 0; ; page++) {
    const pageUrl = `${url}?page=${page}`
    let response = null
    try {
      response = await component.fetch(pageUrl)
    } catch (error) {
      throw `Error while fetching category at ${pageUrl}: ${error}`
    }
    let data = null
    try {
      data = await response.json()
    } catch (error) {
      throw `Error while converting category at ${pageUrl} to JSON: ${error}`
    }
    if (category === null) {
      category = data
    } else {
      category.topic_list.topics = [...category.topic_list.topics, ...data.topic_list.topics]
    }
    if (data.topic_list.topics.length < data.topic_list.per_page) {
      return category
    }
  }
}

export async function fetchDiscourseTopic(component, url) {
  let response = null
  try {
    response = await component.fetch(url)
  } catch (error) {
    throw `Error while fetching topic at ${url}: ${error}`
  }
  try {
    return await response.json()
  } catch (error) {
    throw `Error while converting topic at ${url} to JSON: ${error}`
  }
}

export async function fetchGitLabGroupProjects(component, url) {
  let response = null
  try {
    response = await component.fetch(url)
  } catch (error) {
    throw `Error while fetching GitLab group at ${url}: ${error}`
  }
  try {
    return await response.json()
  } catch (error) {
    throw `Error while converting GitLab group at ${url} to JSON: ${error}`
  }
}
