const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const config = require("sapper/webpack/config.js")
const sass = require("node-sass")
const pkg = require("../package.json")

const entry = config.server.entry()
entry.server = ["babel-polyfill", entry.server]

module.exports = {
  entry,
  output: config.server.output(),
  target: "node",
  resolve: {
    extensions: [".js", ".json", ".html"],
  },
  externals: Object.keys(pkg.dependencies),
  module: {
    rules: [
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: {
          loader: "svelte-loader",
          options: {
            css: false,
            generate: "ssr",
            preprocess: {
              style: ({ content, attributes }) => {
                if (attributes.type !== "text/scss") return

                return new Promise((fulfil, reject) => {
                  sass.render(
                    {
                      data: content,
                      includePaths: ["components", "routes"],
                      sourceMap: true,
                      importer: function(url) {
                        if (url.indexOf("~") === 0) {
                          const nodeModulePath = `./node_modules/${url.substr(1)}`
                          return { file: require("path").resolve(nodeModulePath) }
                        }
                        return { file: url }
                      },
                      outFile: "x", // this is necessary, but is ignored
                    },
                    (err, result) => {
                      if (err) {
                        return reject(err)
                      }
                      fulfil({
                        code: result.css,
                        map: result.map,
                      })
                    }
                  )
                })
              },
            },
          },
        },
      },
      {
        test: /\.(scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader", // translates CSS into CommonJS modules
          {
            loader: "postcss-loader", // Run post css actions
            options: {
              plugins: function() {
                // post css plugins, can be exported to postcss.config.js
                return [require("precss"), require("autoprefixer")]
              },
            },
          },
          "sass-loader", // compiles Sass to CSS
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "../assets/[name].css",
      chunkFilename: "[id].css",
    }),
  ],
  mode: process.env.NODE_ENV,
  performance: {
    hints: false, // it doesn't matter if server.js is large
  },
}
