const webpack = require("webpack")
const config = require("sapper/webpack/config.js")
const sass = require("node-sass")

const mode = process.env.NODE_ENV
const isDev = mode === "development"

const entry = config.client.entry()
entry.main = ["babel-polyfill", entry.main]

module.exports = {
  entry,
  output: config.client.output(),
  resolve: {
    extensions: [".js", ".json", ".html"],
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["env", "stage-0"],
            },
          },
          {
            loader: "svelte-loader",
            options: {
              hydratable: true,
              hotReload: true,
              preprocess: {
                style: ({ content, attributes }) => {
                  if (attributes.type !== "text/scss") return

                  return new Promise((fulfil, reject) => {
                    sass.render(
                      {
                        data: content,
                        includePaths: ["components", "routes"],
                        sourceMap: true,
                        importer: function(url) {
                          if (url.indexOf("~") === 0) {
                            const nodeModulePath = `./node_modules/${url.substr(1)}`
                            return { file: require("path").resolve(nodeModulePath) }
                          }
                          return { file: url }
                        },
                        outFile: "x", // this is necessary, but is ignored
                      },
                      (err, result) => {
                        if (err) {
                          return reject(err)
                        }
                        fulfil({
                          code: result.css,
                          map: result.map,
                        })
                      }
                    )
                  })
                },
              },
            },
          },
          {
            loader: "eslint-loader",
            options: {
              emitWarning: isDev,
              fix: true,
            },
          },
        ],
      },
    ],
  },
  mode,
  plugins: [
    isDev && new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      "process.browser": true,
      "process.env.NODE_ENV": JSON.stringify(mode),
    }),
  ].filter(Boolean),
  devtool: isDev && "inline-source-map",
}
